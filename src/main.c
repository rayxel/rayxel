/*
	rayxel - src/main.c
	
	Copyright (C) 2023 hexaheximal

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <raylib.h>
#include <stdio.h>
#include <unistd.h>

// This function converts a position in screen space into a position in texture space
// See: https://en.wikipedia.org/wiki/Texel_(graphics)

Vector2 pixelToTexel(Vector2 position, Rectangle textureArea, float scale) {
	return (Vector2) { (int)((position.x - textureArea.x) / scale), (int)((position.y - textureArea.y) / scale) };
}

// This function converts a position in texture space into a position in screen space
// See: https://en.wikipedia.org/wiki/Texel_(graphics)

Vector2 texelToPixel(Vector2 position, Rectangle textureArea, float scale) {
	return (Vector2) { textureArea.x + (position.x * scale), textureArea.y + (position.y * scale) };
}

int main(int argc, char** argv) {
	#ifndef DEBUG
		SetTraceLogLevel(LOG_ERROR);
	#endif

	if (argc != 2) {
		puts("No filename specified.");
		puts("Usage: rayxel [filename]");
		return 1;
	}

	char* filename = argv[1];

	if (access(filename, F_OK) == -1 ) {
		printf("ERROR: %s does not exist.\n", filename);
		return 1;
	}

	InitWindow(1280, 720, "rayxel");

	SetWindowState(FLAG_WINDOW_RESIZABLE);

	SetTargetFPS(60);

	SetExitKey(KEY_NULL);

	Image image = LoadImage(filename);
	Texture2D texture = LoadTextureFromImage(image);

	int width, height;

	int scale = 1;

	Vector2 offset = { 0.0f, 0.0f };
	Vector2 lastMousePosition;

	bool isMoving = false;

	while (!WindowShouldClose()) {
		scale += GetMouseWheelMove();

		if (scale > 100) {
			scale = 100;
		}

		if (1 > scale) {
			scale = 1;
		}

		width = GetRenderWidth();
		height = GetRenderHeight();

		Vector2 position = { ((width / 2) - ((texture.width * scale) / 2)) + offset.x, ((height / 2) - ((texture.height * scale) / 2)) + offset.y };

		Rectangle textureArea = { position.x, position.y, texture.width * scale, texture.height * scale };

		BeginDrawing();

		ClearBackground((Color) { 212, 212, 212, 255 }); // Tailwind CSS neutral-300
		DrawRectangleRec(textureArea, (Color) { 255, 255, 255, 255 });

		DrawTextureEx(texture, position, 0.0f, scale, WHITE);

		if (IsKeyDown(KEY_R) && !isMoving) {
			scale = 1;
			offset.x = 0.0f;
			offset.y = 0.0f;
		}

		if (!IsKeyDown(KEY_LEFT_SHIFT) && CheckCollisionPointRec(GetMousePosition(), textureArea)) {
			Vector2 selectedTexel = pixelToTexel(GetMousePosition(), textureArea, scale);

			Vector2 selectedTexelInScreenSpace = texelToPixel(selectedTexel, textureArea, scale);

			DrawRectangleV(selectedTexelInScreenSpace, (Vector2) {scale, scale}, (Color) { 0, 0, 0, 255 });
		}

		if (IsKeyDown(KEY_LEFT_SHIFT) && IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
			if (!isMoving) {
				lastMousePosition = GetMousePosition();
				isMoving = true;
			}

			Vector2 mousePosition = GetMousePosition();

			offset.x += mousePosition.x - lastMousePosition.x;
			offset.y += mousePosition.y - lastMousePosition.y;

			lastMousePosition = mousePosition;
		}

		if (!IsMouseButtonDown(MOUSE_BUTTON_LEFT) && isMoving) {
			isMoving = false;
		}

		#ifdef DEBUG
		// Draw some debug info

		DrawRectangle(0, 0, 256, 200, (Color) { 255, 255, 255, 200 });

		DrawText(TextFormat("Scale: %d", scale), 32, 32, 40, BLACK);
		DrawText(TextFormat("X: %d", (int)offset.x), 32, 32 + 40, 40, BLACK);
		DrawText(TextFormat("Y: %d", (int)offset.y), 32, 32 + 40 + 40, 40, BLACK);
		#endif

		EndDrawing();
	}

	UnloadImage(image);
	UnloadTexture(texture);

	CloseWindow();

	return 0;
}