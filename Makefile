# rayxel - Makefile
#
# Copyright (C) 2023 hexaheximal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

BUILD_TARGET ?= debug

include target/$(BUILD_TARGET).mk

.PHONY: all clean clean_deps

CC ?= gcc

RAYXEL_SRC=$(shell find src -name "*.c")
RAYXEL_OBJ=$(RAYXEL_SRC:src/%.c=bin/%.o)

LIBRAYXEL_SRC=$(shell find src/librayxel -name "*.c")
LIBRAYXEL_OBJ=$(LIBRAYXEL_SRC:src/%.c=bin/%.o)

CFLAGS=$(EXTRA_CFLAGS) -Wall -Werror -Iexternal/raylib/src

all: bin/rayxel

bin/%.o: src/%.c
	@if [ ! -d $(shell dirname $@) ]; then mkdir -p $(shell dirname $@); fi
	gcc -fpic $(CFLAGS) -o $@ -c $^

external/raylib/src/libraylib.a:
	+RAYLIB_BUILD_MODE=$(RAYLIB_BUILD_MODE) make -C external/raylib/src

bin/rayxel: external/raylib/src/libraylib.a $(RAYXEL_OBJ)
	$(CC) $(RAYXEL_OBJ) external/raylib/src/libraylib.a -lm $(CFLAGS) -o $@

bin/librayxel.so: $(LIBRAYXEL_OBJ)
	$(CC) -shared -o bin/librayxel.so $(LIBRAYXEL_OBJ)

bin/librayxel.a: $(LIBRAYXEL_OBJ)
	$(AR) q bin/librayxel.a $(LIBRAYXEL_OBJ)

librayxel: bin/librayxel.so bin/librayxel.a

test: bin/rayxel
	valgrind --leak-check=full ./bin/rayxel

clean_rayxel:
	rm -rf bin/*

clean_deps:
	+make -Cexternal/raylib/src clean

clean: clean_rayxel clean_deps